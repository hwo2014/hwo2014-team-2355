package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.google.gson.Gson;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);
	Race race = new Race();
	CarPositionsMsgData carPositionsData = new CarPositionsMsgData();
	YourCarMsgData yourCarData = new YourCarMsgData();
	TurboMsgData yourTurboData = new TurboMsgData();
	boolean turboIsAvailable = false;
	String yourCarColor = "";
	List<CarPositionsData> carPositions;
	int lastTickSwitchedOn=0;
	int currentTick = 0;
	double slipAngle = 0;
	double currentSpeed = 0;
	//List<Piece> trackPieces;

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
	    if(msgFromServer.msgType.equals("yourCar")){
		YourCarMsgData yourCarMsg = gson.fromJson(line, YourCarMsgData.class);
		yourCarData.setData(yourCarMsg.getData());
		yourCarColor = yourCarData.getData().getColor();
	    }
            if (msgFromServer.msgType.equals("carPositions")) {
		CarPositionsMsgData carPositionsMsg = gson.fromJson(line, CarPositionsMsgData.class);
		carPositionsData.setData(carPositionsMsg.getData());
		carPositions = carPositionsData.getData();


		int myCarPieceIndex = 0;
		int myCarCurrentRank = 0;
		int currentLane = 0;
		boolean canSwitch = false;
		currentTick = carPositionsMsg.getGameTick();
	    

		for(int i = 0; i < carPositions.size(); i++){
		    if(carPositions.get(i).getID().getColor().equalsIgnoreCase(yourCarColor)){
			myCarCurrentRank = i;
			myCarPieceIndex = carPositions.get(i).getPiecePosition().getPieceIndex();
			currentLane = carPositions.get(i).getPiecePosition().getInPieceLane().getEndLaneIndex();	
			slipAngle = carPositions.get(i).getAngle();
		    }
		}

		System.out.println("My car is at track piece number: " + myCarPieceIndex);
		System.out.println("This is the current slip angle: " + slipAngle);


		//deal with which piece the car is on
		if(myCarPieceIndex == (race.getTrack().getPieces().size()-1)){
		    System.out.println("setting index to -1");
		    myCarPieceIndex = -1;
		}

		//deal with turbo
		//if(slipAngle > 20){
		
		//}
		
		//deal with changing lanes
		if(myCarPieceIndex==-1){
		    if(race.getTrack().getPieces().get(0).getCanSwitch()==true){
			canSwitch = true;
		    }
		}
		else if(race.getTrack().getPieces().get(myCarPieceIndex).getCanSwitch()==true){
		    canSwitch = true;
		}

		if((race.getTrack().getPieces().get(myCarPieceIndex +1).getCanSwitch()==true)){
			    canSwitch = true;
	       	}
		//else if((race.getTrack().getPieces().get(myCarPieceIndex +1).getAngle()==0)||(race.getTrack().getPieces().get(myCarPieceIndex).getAngle()==0)&&(race.getTrack().getPieces().get(myCarPieceIndex +1).getLength()<20)){
		//send(new Throttle(.67));
		//}
		if(canSwitch == true){
		    //right turn
		    if(race.getTrack().getPieces().get(myCarPieceIndex +1).getAngle()<0){
			if(currentLane != (race.getTrack().getLanes().size()-1)){
			    send(new LaneSwitch("Right"));
			    System.out.println("switching lanes...");
			    lastTickSwitchedOn = currentTick;
			}
		    }
		    //left turn
		    if(race.getTrack().getPieces().get(myCarPieceIndex +1).getAngle()>0){
			if(currentLane != 0){
			    System.out.println("switching lanes...");
			    send(new LaneSwitch("Left"));
			    lastTickSwitchedOn = currentTick;
			}
		    }
		}

		//if nothing else send new throttle
		send(new Throttle(0.64));


            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
		InitMsgData initMsg = gson.fromJson(line, InitMsgData.class);		
		race.setTrack(initMsg.getData().getRace().getTrack());
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
		send(new Throttle(0.655));
	    } else if (msgFromServer.msgType.equals("crash")) {
		System.out.println("your car has crashed =(");
            } else if(msgFromServer.msgType.equals("turboAvailable")) {
		TurboMsgData turboData = gson.fromJson(line, TurboMsgData.class);
		yourTurboData.setData(turboData.getData());
		turboIsAvailable = true;
		System.out.println("Turbo is now available");
	    } else {
		//System.out.println("" + msgFromServer.msgType);
		//send(new Throttle(0.65));
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class TurboMsgData {
    private Turbo data;

    public Turbo getData() {
	return this.data;
    }
    public void setData(Turbo data){
	this.data = data;
    }
}

class Turbo {
    private double turboDurationMilliseconds;
    private int turboDurationTicks;
    private double turboFactor;

    public double getTurboDurationMilliseconds(){
	return this.turboDurationMilliseconds;
    }
    public int getTurboDurationTicks(){
	return this.turboDurationTicks;
    }
    public double getTurboFactor(){
	return this.turboFactor;
    }

    public void setTurboDurationMilliseconds(double turboDurationMilliseconds){
	this.turboDurationMilliseconds = turboDurationMilliseconds;
    }
    public void setTurboDurationTicks(int turboDurationTicks){
	this.turboDurationTicks = turboDurationTicks;
    }
    public void setTurboFactor(double turboFactor){
	this.turboFactor = turboFactor;
    }
}

class CarPositionsMsgData {
    private List<CarPositionsData> data;
    private int gameTick;

    public int getGameTick(){
	return this.gameTick;
    }
    public List<CarPositionsData> getData(){
	return this.data;
    }
    public void setData(List<CarPositionsData> data) {
	this.data = data;
    }
    public void setGameTick(int gameTick){
	this.gameTick = gameTick;
    }
}

class CarPositionsData {
    private ID id;
    private double angle;
    private PiecePosition piecePosition;
    private int lap;
    public ID getID(){
	return this.id;
    }
    public double getAngle(){
	return this.angle;
    }
    public PiecePosition getPiecePosition(){
	return this.piecePosition;
    }
    public int getLap(){
	return this.lap;
    }
    public void setID(ID id){
	this.id = id;
    }
    public void setAngle(double Angle){
	this.angle = angle;
    }
    public void setPiecePosition(PiecePosition piecePosition){
	this.piecePosition = piecePosition;
    }
    public void setLap(int lap){
	this.lap = lap;
    }
} 

class ID {
    private String name;
    private String color;

    public String getName(){
	return this.name;
    }
    public String getColor(){
	return this.color;
    }

}

class PiecePosition {
    private int pieceIndex;
    private double inPieceDistance;
    private InPieceLane lane;

    public int getPieceIndex(){
	return this.pieceIndex;
    }
    public double getInPieceDistance(){
	return this.inPieceDistance;
    }
    public InPieceLane getInPieceLane(){
	return this.lane;
    }
    public void setPieceIndex(int pieceIndex){
	this.pieceIndex = pieceIndex;
    }
    public void setInPieceDistance(double inPieceDistance){
	this.inPieceDistance = inPieceDistance;
    }
    public void setInPieceLane(InPieceLane lane){
	this.lane = lane;
    }
}

class InPieceLane {
    private int startLaneIndex;
    private int endLaneIndex;

    public int getStartLaneIndex(){
	return this.startLaneIndex;
    }
    public int getEndLaneIndex(){
	return this.endLaneIndex;
    }
    public void setStartLaneIndex(int startLaneIndex){
	this.startLaneIndex = startLaneIndex;
    }
    public void setEndLaneIndex(int endLaneIndex){
	this.endLaneIndex = endLaneIndex;
    }
}

class YourCarMsgData {
    private YourCarData data;

    public YourCarData getData(){
	return this.data;
    }
    public void setData(YourCarData data){
	this.data = data;
    }
}

class YourCarData {
    private String name;
    private String color;

    public String getName(){
	return this.name;
    }
    public String getColor(){
	return this.color;
    }
    public void setName(String name){
	this.name = name;
    }
    public void setColor(String color){
	this.color = color;
    }
}


class InitMsgData {
    private InitData data;

    public InitData getData(){
	return this.data;
    }
    public void setData(InitData data){
	this.data = data;
    }
}

class InitData {
    private Race race;
    
    public Race getRace(){
	return this.race;
    }
    public void setRace(Race race){
	this.race = race;
    }
}

class Race {
    private Track track;
    private Object cars;

    public Track getTrack(){
	return this.track;
    }
    public void setTrack(Track track){
	this.track = track;
    }
    public Object getCars(){
	return this.cars;
    }
    public void setCars(Object cars){
	this.cars = cars;
    }
}

class Track {
    private List<Piece> pieces;
    private List<Lane> lanes;
    private Object startingPoint;
    private Object raceSession;
    //private Object throttle;

    public List<Piece> getPieces(){
	return this.pieces;
    }
    public List<Lane> getLanes(){
	return this.lanes;
    }
    public void setPieces(List<Piece> pieces){
	this.pieces = pieces;
    }
    public void setLanes(List<Lane> lanes){
	this.lanes = lanes;
    }
}


class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class LaneSwitch extends SendMsg {
    private String value;

    public LaneSwitch(String value){
	this.value = value;
    }

    @Override 
    protected Object msgData() {
	return value;
    }
    
    @Override
    protected String msgType(){
	return "switchLane";
    }
}

class Piece {
    private boolean highCrashChance = true;
    private double length;
    private int radius;
    private double angle=0;
    @SerializedName("switch")
    private boolean canSwitch;

    public void setHighCrashChance(boolean highCrashChance){
	this.highCrashChance = highCrashChance;
    }
    public void setLength(double length){
	this.length = length;
    }
    public void setRadius(int radius){
	this.radius = radius;
    }
    public void setAngle(double angle){
	this.angle = angle;
    }
    public void setCanSwitch(boolean canSwitch){
	this.canSwitch = canSwitch;
    }

    public double getLength(){
	return this.length;
    }
    public int getRadius(){
	return this.radius;
    }
    public double getAngle(){
	return this.angle;
    }
    public boolean getCanSwitch(){
	return this.canSwitch;
    }
    public boolean getHighCrashChance(){
	return this.highCrashChance;
    }
}

class Lane {
    private int distanceFromCenter;
    private int index;

    public int getDistanceFromCenter(){
	return this.distanceFromCenter;
    }
    public int getIndex(){
	return this.index;
    }

    public void setDistanceFromCenter(int distanceFromCenter){
	this.distanceFromCenter = distanceFromCenter;
    }
    public void setIndex(int index){
	this.index = index;
    }
}